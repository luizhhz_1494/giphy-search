import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';



import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GiphySearchManualComponentModule } from './giphy/giphy-search-manual/giphy-search-manual.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    GiphySearchManualComponentModule    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
