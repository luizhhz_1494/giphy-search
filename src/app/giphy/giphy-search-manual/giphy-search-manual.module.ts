import { NgModule } from '@angular/core';

import { GiphySearchManualComponent } from './giphy-search-manual.component';
import { GiphySearchService } from '../giphy-search.service';



@NgModule({
  declarations: [GiphySearchManualComponent],
  imports:  [],
  providers: [GiphySearchService], //Sempre que adicionar um Service deverá ser adicionado aqui
  exports: [GiphySearchManualComponent]
})
export class GiphySearchManualComponentModule { 

}
